/**
 * Created by Cormac on 08/09/2017.
 */
import React, {Component} from "react";
import {Grid, Row, Col} from "react-bootstrap";
import {Card, CardTitle, CardActions, CardHeader, CardText} from "material-ui/Card";
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from "material-ui/Table";
import CardReactFormContainer from "card-react";
import "./card.css";
import TextField from "material-ui/TextField";
import Checkbox from "material-ui/Checkbox";
import "./CardForm.css";

class CardForm extends Component {
    constructor(props) {
        super(props);

        this.createCard = false;
    }

    randomNumberBetween0and19 = Math.floor(Math.random() * 20);

    randomWholeNum() {
        return Math.random();
    }

    onBlur() {
        //if (this.props.requiresLayoutUpdate != null) this.props.requiresLayoutUpdate();
    }

    render() {
        const cardWrapperID = "card-wrapper" + this.randomWholeNum();
        const textFieldStyle = {width: "100%"};
        return (
            <div>
                <div className="genericCard" id={cardWrapperID}></div>
                <CardReactFormContainer
                    container={cardWrapperID}
                    formInputsNames={
                        {
                            number: 'CCnumber',
                            expiry: 'CCexpiry',
                            cvc: 'CCcvc',
                            name: 'CCname'
                        }
                    }
                    classes={
                        {
                            valid: 'valid-input',
                            invalid: 'invalid-input'
                        }
                    }
                    formatting={true} >
                    <Grid style = {{maxWidth: 550}}>
                        <Row style={{textAlign: "center"}}>
                            <Col md="6">
                                <TextField
                                    floatingLabelText="Full name"
                                    name="CCname"
                                    style={textFieldStyle}
                                    onChange={(event) => {
                                        this.nameValue = event.target.value;
                                    }}
                                />
                            </Col>
                            <Col md="6">
                                <TextField
                                    floatingLabelText="Card number"
                                    name="CCnumber"
                                    style={textFieldStyle}
                                    onChange={(event) => {
                                        this.numberValue = event.target.value;
                                    }}
                                />
                            </Col>
                            <Col md="6" xs="6">
                                <TextField
                                    floatingLabelText="MM/YY"
                                    name="CCexpiry"
                                    style={textFieldStyle}
                                    onChange={(event) => {
                                        this.expiryValue = event.target.value;
                                    }}
                                />
                            </Col>
                            <Col md="6" xs="6">
                                <TextField
                                    floatingLabelText="CVC"
                                    name="CCcvc"
                                    style={textFieldStyle}
                                    onChange={(event) => {
                                        this.cvcValue = event.target.value;
                                    }}
                                />
                            </Col>
                        </Row>
                        { this.props.saveCard ?
                            <Row>
                                <Col md="4" sm="4" xs="3"/>
                                <Col md="4" xs="8">
                                    <Checkbox
                                        onCheck={(e, checked) => {
                                            this.createCard = checked;
                                        }}
                                        style={
                                            {
                                                paddingTop: 15,
                                                block: {
                                                    maxWidth: 250
                                                },
                                                checkbox: {
                                                    marginBottom: 16,
                                                },
                                            }
                                        }
                                        label="Save Card"
                                    />
                                </Col>
                            </Row> : null
                        }
                        <Row>
                            <p className="smallText">Saved cards are encrypted &amp; unreadable. They can only be unencrypted &amp; readable with your MoneyFeel password.</p>
                        </Row>
                        {this.props.children}
                    </Grid>

                </CardReactFormContainer>
            </div>
        )
    }
}

export default CardForm;