import React, {Component} from "react";
import {Route, Link, HashRouter} from "react-router-dom";
import AuthenticationFrame from "./AuthenticationFrame";

function scrollToTop(scrollDuration) {

    var cosParameter = document.documentElement.scrollTop / 2,
        scrollCount = 0,
        oldTimestamp = performance.now();
    function step (newTimestamp) {
        scrollCount += Math.PI / (scrollDuration / (newTimestamp - oldTimestamp));
        if (scrollCount >= Math.PI) window.scrollTo(0, 0);
        if (document.documentElement.scrollTop === 0) return;
        window.scrollTo(0, Math.round(cosParameter + cosParameter * Math.cos(scrollCount)));
        oldTimestamp = newTimestamp;
        window.requestAnimationFrame(step);
    }
    window.requestAnimationFrame(step);

}

const renderMergedProps = (component, ...rest) => {
    const finalProps = Object.assign({}, ...rest);
    return (
        React.createElement(component, finalProps)
    );
};

export const PropsRoute = ({ component, ...rest }) => {
    return (
        <Route {...rest} render={routeProps => {
            scrollToTop(700);
            return renderMergedProps(component, routeProps, rest);
        }} />
    );
};

export const AuthenticationPropsRoute = ({ component, ...rest }) => {
    // if (preRender != null) preRender();
    return (
            <Route {...rest} render={routeProps => {
                scrollToTop(700);
                const all = renderMergedProps(component, routeProps, rest);
                return <AuthenticationFrame parent = {all}>{all}</AuthenticationFrame>
            }} />
    );
};