/**
 * Created by Cormac on 21/08/2017.
 */
import React, {Component} from "react";
import {Grid, Row, Col} from "react-bootstrap";
import {Route, Link, HashRouter} from "react-router-dom";

const rowMargin = 25;

class Footer extends Component {
    render() {
        return (
            <footer style={{color: "white", backgroundColor: "#000000", paddingTop: 20, paddingBottom: 30}}>
                <Grid style = {{textAlign: "center", maxWidth: 320}}>
                    <Row></Row>
                    <Row style = {{marginTop: rowMargin}}>
                        <Col md="4"><Link to="/aboutUs" style = {{color: "white"}}>About Us</Link></Col>
                        <Col md="4"><Link to="/contactUs" style = {{color: "white"}}>Contact Us</Link></Col>
                        <Col md="4"><Link to="/tAndC" style = {{color: "white"}}>Terms</Link></Col>
                    </Row>
                    <Row style = {{marginTop: rowMargin}}>
                        <Col xs="4"><a target="_blank"  href="https://www.facebook.com/hellomoneyfeel/"><img width={35} src={resource("facebookwhite11.png")}/></a></Col>
                        <Col xs="4"><a target="_blank" href="https://twitter.com/hellomoneyfeel"><img width={35} src={resource("twitter11.png")}/></a></Col>
                        <Col xs="4"><a target="_blank" href="https://www.linkedin.com/company-beta/10990807/"><img width={35} src={resource("linkedin11.png")}/></a></Col>
                    </Row>
                    <Row style = {{marginTop: rowMargin}}>
                        © Moneyfeel 2017
                    </Row>
                </Grid>
            </footer>
        )
    }
}

export default Footer;