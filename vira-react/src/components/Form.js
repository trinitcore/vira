/**
 * Created by Cormac on 31/07/2017.
 */
import React, {Component} from "react";
import {Grid, Row, Col} from "react-bootstrap";
import TextField from "material-ui/TextField";
import {Card, CardActions, CardHeader, CardText} from "material-ui/Card";
import RaisedButton from "material-ui/RaisedButton";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";
import CircularProgress from "material-ui/CircularProgress";
import ArrowBack from "react-material-icons/icons/navigation/arrow-back";
import {Route, Link, HashRouter} from "react-router-dom";
import IconButton from "material-ui/IconButton";
import qs from "qs";
import axios from "axios";
import Checkbox from "material-ui/Checkbox";
import TimePicker from "material-ui/TimePicker";
import DatePicker from "material-ui/DatePicker";
import FloatingActionButton from 'material-ui/FloatingActionButton';
import DeleteIcon from "material-ui/svg-icons/action/delete-forever";
import AddIcon from "material-ui/svg-icons/content/add";

const row = {
    paddingTop: 8,
    paddingBottom: 8
};
const rowNoTopPadding = {
    paddingBottom: 8
};

const container = {
    marginBottom: "16px",
    width: "100%",
    maxWidth: "390px"
};

const centerContainer = {
    container,
    marginTop: "50px"
};

const statusText = "statusText";
const buttonWrapper = "buttonWrapper";
const linkWrapText = "linkWrapText";
const button = "button";
const field = "field";
const pairs = "pairs";
const checkBox = "checkBox";
const selectField = "selectField";

export class Form extends Component {

    formParameters = {
        field : [],
        selectField: [],
        checkBoxes: [],
        pairs: [],
        uploadFields: [],

        buttonWrapper: null,
        submitButtons: []
    };


    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.props.immediateSubmit == true) {
            this.handleFormSubmit();
        }
    }

    createFilesPostRequest = (fields, selectFields, files) => {
        var formData = new FormData();

        Object.keys(fields).forEach((key, index) => {
            const value = fields[key];
           formData.append(key, value);
        });

        Object.keys(selectFields).forEach((key, index) => {
            const value = selectFields[key];
            value.forEach((selectedValue) => {
                formData.append(key, selectedValue);
            });
        });

        if (files != null) {
            Object.keys(files).forEach((key, index) => {
                const value = files[key];
                formData.append(key, value);
            });
        }

        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        };
        const request = axios.post(this.props.URL, formData);
        return request;
    };

    handleFormSubmit = () => {
        var alLFieldsSatisfied = true;
        var errorMessage = "You left a field blank.";

        // Text fields
        var textfieldFormMap = {};
        this.formParameters.field.forEach(field => {
            if (field.isBlank() && !field.isOptional()) {
                alLFieldsSatisfied = false;
            } else {
                textfieldFormMap[field.props.name] = field.getValue();
            }
        });

        // Select fields
        var selectFieldQSString = "";
        var selectFieldFormMap = {};
        this.formParameters.selectField.forEach(field => {
            field.getValues().forEach(value => {
                selectFieldQSString += "&" + field.props.name + "=" + value;
            });
            if (field.state.multiple) selectFieldFormMap[field.props.name] = field.getValues();
            else selectFieldFormMap[field.props.name] = field.getValues()[0];
            if (selectFieldQSString == "") {
                alLFieldsSatisfied = false;
                errorMessage = "You didn't select any values for " + field.props.label;
            }
        });

        // Checkboxes
        var checkboxFormMap = {};
        this.formParameters.checkBoxes.forEach(field => {
            if (field.props.mustBeTrue == true) {
                if (field.getValue() == false) {
                    alLFieldsSatisfied = false;
                    errorMessage = field.props.errorIfFalse;
                }
            }
            checkboxFormMap[field.props.name] = field.getValue();
        });

        // File uploads
        var filesFormMap = {};
        var isFileUpload = false;
        this.formParameters.uploadFields.forEach(field => {
            isFileUpload = true;
            filesFormMap[field.props.name] = field.getValue();
        });

        // Pairs
        var pairsFormMap = {};
        this.formParameters.pairs.forEach(field => {
            pairsFormMap[field.props.name] = field.getValues()
        });

        if (alLFieldsSatisfied) {
            // Set in progress
            this.formParameters.buttonWrapper.setState({inProgress: true, status: null});

            // If it has a custom form submit function
            if (this.props.onFormSubmit != null) {
                const onFormSubmit = this.props.onFormSubmit;
                onFormSubmit(Object.assign(textfieldFormMap, selectFieldFormMap, checkboxFormMap), (status, statusMessage) => {
                    let success = status == "green";
                    this.formParameters.buttonWrapper.setState({
                        inProgress: false,
                        hideButtons: success ? this.props.postRequestHideButtonWrapper : false,
                        status: status,
                        statusMessage: statusMessage
                    });
                });
            } else {
                // Gather all text field parameters
                var fullTextFieldParameters;
                if (this.props.additionalParameters != null) {
                    fullTextFieldParameters = Object.assign(this.props.additionalParameters, textfieldFormMap);
                } else {
                    fullTextFieldParameters = textfieldFormMap;
                }

                console.log("JSON Parameters:");
                const jsonParameters = Object.assign(fullTextFieldParameters, checkboxFormMap, selectFieldFormMap, pairsFormMap);
                console.log(jsonParameters);

                var promise;
                if (isFileUpload) promise = this.createFilesPostRequest(Object.assign(fullTextFieldParameters,checkboxFormMap), selectFieldFormMap, filesFormMap);
                else promise = axios.post(this.props.URL, jsonParameters);

                promise.then(response => {
                    let data = response.data;
                    let success = data.ok;
                    let status = success ? "green" : "red";
                    console.log(status);

                    var shouldContinue = true;
                    if (this.props.postRequestHandler != null) {
                        shouldContinue = this.props.postRequestHandler(success, data.message, this.formParameters.buttonWrapper, this.formParameters, data);
                    }
                    if (shouldContinue) {
                        this.formParameters.buttonWrapper.setState({
                            inProgress: false,
                            hideButtons: success ? this.props.postRequestHideButtonWrapper : false,
                            status: status,
                            statusMessage: data.message
                        });
                    }
                })
            }
        } else {
            this.formParameters.buttonWrapper.setState({
                inProgress: false,
                status: "red",
                statusMessage: errorMessage
            });
        }
    };

    render() {
        const mainElement = [
            <Col style={this.props.center ? centerContainer : container} md={4} sm={4} xs={12}>
                <div>
                    <Card style={{}}>
                        <CardHeader
                            style={row}
                            textStyle={{paddingRight: 0}}
                            avatar={(this.props.backButton == true) ? <IconButton onClick={()=> this.props.parent.props.history.goBack()}><ArrowBack /></IconButton> : <span></span>}
                            title={<h5 style={{marginTop: 13, whiteSpace: "nowrap"}}>{this.props.title}</h5>}
                        />
                        <Grid fluid={true} style = {{textAlign: "center"}}>
                            {React.Children.map(this.props.children, (e => {
                                if (e != null) {
                                    return React.cloneElement(e,
                                        {formParent: this}
                                    )
                                }
                                return e;
                            }))}
                        </Grid>
                    </Card>
                </div>
            </Col>
        ];

        return (
            <form>
                {(this.props.center == true) ?
                <Grid fluid = {!this.props.center}>
                    <Row>
                        <Col md={4} sm={4}/>
                        {mainElement}
                    </Row>
                </Grid> : mainElement}
            </form>
        )
    }
}

class FormSubComponent extends Component {
    getElementType = () => {
        return this.formElementType;
    };

    setValue = (value) => {
        this.setState({value: value});
    };

    getValue = () => {
        return this.state.value
    }
}

export class FormPairs extends FormSubComponent {

    formParameters = {
        field : [],
        selectField: [],
        checkBoxes: [],
        pairs: [],
        uploadFields: [],

        buttonWrapper: null,
        submitButtons: []
    };

    currentPairs = [];

    optional = false;
    constructor(props) {
        super(props);

        for (var i = 0; i < this.props.pairNames.length; i++) {
            let name = this.props.pairNames[i];
            this.currentPairs.push(
                {
                    pairName: name,
                    formParameters : {
                        field : [],
                        selectField: [],
                        checkBoxes: [],
                        pairs: [],
                        uploadFields: [],

                        buttonWrapper: null,
                        submitButtons: []
                    }
                }
            );
        }

        this.state = {
            values: (props.defaultValues != null) ? props.defaultValues :
                []
        };
        this.formElementType = pairs;

        props.formParent.formParameters.pairs.push(this);
        if (props.optional != null) this.optional = props.optional;
    }

    getValues = () => {
        return this.state.values;
    };

    addPair = () => {
        var alLFieldsSatisfied = true;

        var textfieldFormMap = {};

        for (var i = 0; i < this.currentPairs.length; i++) {
            var currentPair = this.currentPairs[i];

            currentPair.formParameters.field.forEach(field => {
                if (field.isBlank() && !field.isOptional()) {
                    alLFieldsSatisfied = false;
                } else {
                    textfieldFormMap[currentPair.pairName] = field.getValue();
                }
            });
        }
        if (alLFieldsSatisfied) {
            console.log("All fields satisfied");
            var values = this.state.values;
            values.push(textfieldFormMap);
            this.setState({values: values})
        }
    };

    removePair = (index) => {
        const values = this.state.values;
        values.splice(index,1);
        this.setState({values: values});
    };

    render() {
        return (
            <Row>
                <Row style = {{paddingLeft: 32, paddingRight: 32}}>
                    {this.state.values.map((value,index) =>
                        <Row style = {{marginBottom:8}}>
                            {this.props.pairNames.map(pairName =>
                                <Col xs={5}>
                                    <h4>{value[pairName]}</h4>
                                </Col>
                            )}
                            <Col xs={2}>
                                <FloatingActionButton onClick={() => this.removePair(index)} style={{marginLeft: -16}} mini={true} >
                                    <DeleteIcon/>
                                </FloatingActionButton>
                            </Col>
                         </Row>
                    )}
                    <Row>
                        {this.props.pairComponents.map((comp,index) =>
                                <Col xs={5}>
                                    {React.cloneElement(comp,{formParent: this.currentPairs[index]})}
                                </Col>
                            )}
                        <Col xs={2} style = {{height: "100%"}}>
                            <FloatingActionButton style={{marginLeft: -16, marginTop: 20}} mini={true} onClick={this.addPair}>
                                <AddIcon/>
                            </FloatingActionButton>
                        </Col>
                    </Row>
                </Row>
            </Row>
        )
    }
}

FormPairs.propTypes = {
    pairNames: React.PropTypes.array.isRequired,
    pairComponents: React.PropTypes.array.isRequired,
};

export class FormStatusText extends FormSubComponent {
    formElementType = statusText;

    render() {
        return (
            <Col xs={12} style={{marginBottom: 8}}>
                <span style={{textAlign: "center", color: this.props.status}}>{(this.props.status != null) ? this.props.children: null}</span>
            </Col>
        )
    }
}

export class FormButtonWrapper extends FormSubComponent {

    getSubmitButtons = () => {
        return this.formSubmitButtons;
    };

    setInProgress = () => {
        this.setState({inProgress: true});
    };

    constructor(props) {
        super(props);
        this.state = {
            inProgress: false,
            hideButtons: false
        };

        this.formElementType = buttonWrapper;
        props.formParent.formParameters.buttonWrapper = this;
    }

    componentDidMount() {
        console.log("Child buttons");
    }

    render() {
        const elements =  React.Children.map(this.props.children, (e => {
            if (e != null) {
                return React.cloneElement(e,
                    {wrapperParent: this}
                )
            }
            return e;
        }));

        return (
                <Row style = {row}>
                    {this.state.inProgress ? null : <FormStatusText status={this.state.status}>{this.state.statusMessage}</FormStatusText>}
                    {this.state.inProgress ? <CircularProgress style={{marginBottom: 8, marginTop: 8}} /> : null }
                    {this.state.hideButtons ? null : <Row style={{display: this.state.inProgress ? "none" : "block"}}>{elements}</Row>}
                </Row>
        );
    }
}

export class FormLinkWrapText extends FormSubComponent {
    formElementType = linkWrapText;

    render() {
        return (
            <Row style = {{marginBottom: 2}}>
                <div style={{textAlign: "center"}}>
                    <h6>{this.props.textWrap} <Link to={this.props.linkTo} onClick={this.props.signUpClick} style={{whiteSpace: "nowrap"}}>{this.props.linkText}</Link></h6>
                </div>
            </Row>
        )
    }
}

export class FormButton extends FormSubComponent {

    constructor(props) {
        super(props);
        this.md = (props.md == null) ? 6 : props.md;
        this.xs = (props.xs == null) ? 6 : props.xs;

        this.mdOffset = (props.mdOffset == null) ? null : props.mdOffset;
        this.xsOffset = (props.xsOffset == null) ? null : props.xsOffset;

        this.onSubmit = (props.onSubmit == null) ? props.wrapperParent.props.formParent.handleFormSubmit : props.onSubmit;

        this.formElementType = button;

        console.log(props);
    }

    render() {
        return (
            <Col md = {this.md} xs = {this.xs} mdOffset = {this.mdOffset} xsOffset = {this.xsOffset}>
                <RaisedButton onClick={this.onSubmit} label={this.props.label} primary={true} />
            </Col>
        )
    }
}

export class FormUploadField extends FormSubComponent {

    constructor(props) {
        super(props);
        this.state ={
            file:null
        };
        props.formParent.formParameters.uploadFields.push(this);
    }

    onChange = (e) => {
        this.setState({file:e.target.files[0]})
    };

    getValue = () => {
        return this.state.file;
    };

    render() {
        return (
            <Row>
                <Col xs={12} style={{textAlign: "center"}}>
                    <h6>{this.props.label}</h6>
                    <input type="file" name={this.props.name} onChange={this.onChange}/>
                </Col>
            </Row>
        )
    }

}

export class FormDatePicker extends FormSubComponent {
    optional = false;
    constructor(props) {
        super(props);
        this.state = {
            value: (props.defaultValue != null) ? new Date(props.defaultValue) : null
        };
        this.formElementType = field;

        props.formParent.formParameters.field.push(this);
        if (props.optional != null) this.optional = props.optional;
    }

    handleDateFieldChange = (ee, e) => {
        // Replace method is required to fix known problem in Internet Explorer / Edge where weird Left-to-Right values are being randomly inserted
        this.setState({value: e});
    };

    isOptional = () => {
        return this.optional;
    };

    isBlank = () => {
        return (this.getValue() == null || this.getValue().replace(" ", "") == "")
    };

    getValue = () => {
        return this.state.value.toLocaleDateString('en-GB').replace(/\u200E/g, '')
    };

    render() {
        return (
            <Row>
                <Col md={12} style = {{height: 72, paddingTop: 28}}>
                    <DatePicker value={this.state.value} style={{fontWeight: "bold", position: "relative", bottom: 0}} hintText={this.props.label}
                                formatDate={new Intl.DateTimeFormat('en-GB', {
                                    day: 'numeric',
                                    month: 'numeric',
                                    year: 'numeric',
                                }).format}

                                openToYearSelection={true} onChange={this.handleDateFieldChange} />
                </Col>
            </Row>
        )
    }
}

export class FormTimePicker extends FormSubComponent {

    optional = false;
    constructor(props) {
        super(props);
        this.state = {
            value: props.defaultValue
        };
        this.formElementType = field;

        props.formParent.formParameters.field.push(this);
        if (props.optional != null) this.optional = props.optional;
    }

    handleDateFieldChange = (ee, e) => {
        // Replace method is required to fix known problem in Internet Explorer / Edge where weird Left-to-Right values are being randomly inserted
        console.log(e.toLocaleTimeString('en-GB',{hour: '2-digit', minute:'2-digit'}).replace(/\u200E/g, ''));
        this.setState({value: e.toLocaleTimeString('en-GB',{hour: '2-digit', minute:'2-digit'}).replace(/\u200E/g, '')});
    };

    isOptional = () => {
        return this.optional;
    };

    isBlank = () => {
        return (this.getValue() == null || this.getValue().replace(" ", "") == "")
    };

    getValue = () => {
        return this.state.value
    };

    render() {
        return (
            <Row>
                <Col md={12} style = {{height: 72, paddingTop: 28}}>
                    <TimePicker format={"24hr"} textFieldStyle={{maxWidth: "85%"}} style={{fontWeight: "bold", position: "relative", bottom: 0}} hintText={this.props.label}
                                minutesStep = {1}
                                openToYearSelection={true} onChange={this.handleDateFieldChange}
                                />
                </Col>
            </Row>
        )
    }

}

export class FormField extends FormSubComponent {

    optional = false;
    constructor(props) {
        super(props);
        this.state = {
            value: props.defaultValue
        };
        this.formElementType = field;

        props.formParent.formParameters.field.push(this);
        if (props.optional != null) this.optional = props.optional;
    }

    handleTextFieldChange = (e) => {
        this.setState({value: e.target.value});
    };

    getValue = () => {
        return this.state.value;
    };

    isOptional = () => {
        return this.optional;
    };

    isBlank = () => {
        return (this.getValue() == null || this.getValue().replace(" ", "") == "")
    };

    render() {
        const expanding = [
                    <TextField
                        style = {{textAlign: "left", maxWidth: "85%"}}
                        multiLine = {true}
                        rowsMax = {5}
                        rows={2}
                        defaultValue={this.props.defaultValue}
                        floatingLabelText={this.props.label} onChange={this.handleTextFieldChange}
                    />
        ];

        return (
            <Row>
                <Col md={12}>
                    {this.props.expanding ? expanding :
                        <TextField
                            name={this.props.name}
                            type={this.props.type}
                            style={{maxWidth: "85%"}}
                            defaultValue={this.props.defaultValue}
                            floatingLabelText={this.props.label} onChange={this.handleTextFieldChange}
                        />}
                </Col>
            </Row>
        )
    }
}

export class FormCheckbox extends FormSubComponent {

    constructor(props) {
        super(props);
        this.state = {
            checked: false,
        };

        this.formElementType = checkBox;

        props.formParent.formParameters.checkBoxes.push(this);
    }

    updateCheck() {
        this.setState((oldState) => {
            return {
                checked: !oldState.checked,
            };
        });
    }

    isChecked = () => {
        return this.state.checked;
    };

    getValue = () => {
        return this.state.checked;
    };

    render() {
        return (
            <Row>
                <Col xs={5}></Col>
                <Col xs={6} style={{textAlign: "left", marginTop: 8}}>
                    <Checkbox
                        checked={this.state.checked}
                        onCheck={this.updateCheck.bind(this)}
                    />
                </Col>
                <Col xs={12} style={{marginTop: 6}}>
                    <strong>{this.props.label}</strong>
                </Col>
            </Row>
        )
    }

}

export class FormSelectField extends FormSubComponent {

    constructor(props) {
        super(props);
        const items = this.props.items;
        this.state = {
            values: (props.defaultValues != null) ? props.defaultValues : [],
            items: (items != null) ? items : [],
            multiple: (this.props.multiple != null) ? this.props.multiple : true,

            warningText: (this.props.warningText != null) ? this.props.warningText : null,
            warningTextCondition: (this.props.warningTextCondition != null) ? this.props.warningTextCondition : null,

            showWarningText: false
        };
        this.formElementType = selectField;

        props.formParent.formParameters.selectField.push(this);
    }

    getValues = () => {
        if (this.state.multiple) {
            return this.state.values;
        } else return [this.state.values];
    };

    componentDidMount() {
        if (this.props.itemsRESTURL != null) {
            axios.get(this.props.itemsRESTURL, {withCredentials: true})
                .then(res => {
                    const data = res.data;
                    console.log(data);

                    this.setState({
                        items: data[this.props.itemsRESTObject]
                    });
                    console.log(this.state);
                });
        }
    }

    handleSelectionChange = (event, index, value) => {
        this.setState({values: value, showWarningText: (this.state.warningText != null && this.state.warningTextCondition != null && this.state.warningTextCondition(value) == true)});
        this.categoryID = value;
    };

    selectionRenderer = (values) => {
        if (this.state.multiple) {
            switch (values.length) {
                case 0:
                    return '';
                default:
                    return `${values.length} ${this.props.selectionSuffixText}`;
            }
        } else {
            return this.state.items.find(res => {
                return res.ID == values
            }).name;
        }
    };

    render() {
        return (
            <Row>
                <Col md="12">
                    <SelectField
                        floatingLabelText={this.props.label}
                        value={this.state.values}
                        multiple={this.state.multiple}
                        onChange={this.handleSelectionChange}
                        selectionRenderer={this.selectionRenderer}
                        style = {{maxWidth: 256, textAlign: "left", width: "90%"}} >
                        {this.state.items.map(item =>
                            this.state.multiple ?
                                <MenuItem checked={this.state.values.indexOf(item.ID) > -1}
                                          insetChildren={true}  value={item.ID} primaryText={item.name} /> :
                                <MenuItem insetChildren={true}  value={item.ID} primaryText={item.name} />
                        )}
                    </SelectField>
                    {(this.state.showWarningText) ? <FormStatusText status={"#FFA000"}>{this.state.warningText}</FormStatusText> : null}
                </Col>
            </Row>
        )
    }
}