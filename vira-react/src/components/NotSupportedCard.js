/**
 * Created by Cormac on 22/11/2017.
 */
import React, {Component} from "react";
import {Grid, Row, Col} from "react-bootstrap";
import Avatar from "material-ui/Avatar";
import {Card, CardActions, CardHeader, CardText} from "material-ui/Card";
import AccessTimeIcon from "react-material-icons/icons/device/access-time";
import {detect} from "detect-browser";

class NotSupportedCard extends Component {

    state = {};

    constructor(props) {
        super(props);
        this.browser = detect();

        this.state = {isIncompatible: ((this.browser.name == "edge" || this.browser.name == "ie") || (this.browser.name == "chrome" && this.browser.os == "iOS"))};
    }

    componentDidMount() {

    }

    render() {
        var browsers = null;

        if (this.browser.os == "iOS") {
            browsers = (
                <span><a onClick={() => window.location='googlechrome-x-callback://x-callback-url/open/?url='+encodeURIComponent(location.href)+'&x-source=Safari&x-success='+encodeURIComponent(location.href)}>Open in Safari</a></span>
            )
        } else {
            browsers = (
                <span><a href="https://www.google.com/chrome/browser/desktop/index.html">Download Chrome</a> or <a href="https://www.mozilla.org/en-US/firefox/new/">Download Firefox</a></span>
            )
        }

        return (
            this.state.isIncompatible ?
            <Row style={{marginBottom: 20}}>
                <Card>
                    <CardHeader
                        textStyle={{paddingRight: 0, maxWidth: 400}}
                        title = {<span><strong>Note</strong> Your browser does not support video calling yet.</span>}
                        subtitle = {browsers}
                        paddingRight = {0}
                    />
                </Card>
            </Row> : null
        );
    }
}

export default NotSupportedCard;