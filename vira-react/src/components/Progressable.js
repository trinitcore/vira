import React, {Component} from "react";
import CircularProgress from "material-ui/CircularProgress";

class Progressable extends Component {
    render() {
        return (
            <div>
                {this.props.inProgress ? <div style={{textAlign: "center", marginTop: 30}}>
                    <CircularProgress style={{marginBottom: 1}}/></div> : this.props.children }
            </div>
        )
    }
}

Progressable.propTypes = {
    inProgress: React.PropTypes.bool.isRequired
};

export default Progressable;