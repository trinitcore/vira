/**
 * Created by Cormac on 21/08/2017.
 */
import React, {Component} from "react";
import {Route, Link, HashRouter} from "react-router-dom";

class Title extends Component {

    constructor(props) {
        super(props);
        const fontSize = (props.fontSize == null) ? 38 : props.fontSize;
        const styles = props.style;
        const titleStyle = {
            color: (props.textColor != null) ? props.textColor : "#15b9ed",
            fontSize: 30,
        };

        if (styles != null) {
            this.titleStyle = Object.assign(titleStyle, styles);
        } else {
            this.titleStyle = titleStyle;
        }
    }

    render() {
        const moneyFeelElement = [
            <span style={ this.titleStyle }><strong>Money</strong>Feel</span>
        ];
        return (
            (this.props.homeTapDisabled == false) ?
                <Link to="/" style={{ textDecoration: 'none' }}>{moneyFeelElement}</Link> :
                <span>{moneyFeelElement}</span>
        )
    }
}

export default Title;