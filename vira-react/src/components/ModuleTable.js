/**
 * Created by Cormac on 29/10/2017.
 */
import React, {Component} from "react";
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from "material-ui/Table";
import RaisedButton from "material-ui/RaisedButton";
import {Card, CardTitle, CardActions, CardHeader, CardText} from "material-ui/Card";

export class ModuleTableButton extends Component {

    constructor(props) {
        super(props);
        this.label = props.label;
        this.primary = props.primary;
        this.secondary = props.secondary;
        this.onTouchTap = props.onTouchTap;
    }

    render() {
        return (
            <RaisedButton style={{marginRight: 16}}
                          label={this.label}
                          primary={this.primary}
                          secondary={this.secondary}
                          onClick={this.onTouchTap}/>
        )
    }

}

ModuleTableButton.propType = {
    label: React.PropTypes.string.isRequired,
    primary: React.PropTypes.object,
    secondary: React.PropTypes.object,
    onTouchTap: React.PropTypes.object.isRequired,
};

export class ModuleTable extends Component {

    moduleTitles = [];
    modules = [];
    moduleStructure = [];
    moduleButtons = [(module) => {
    }];
    title = null;

    constructor(props) {
        super(props);
        this.moduleTitles = props.moduleTitles;
        this.modules = props.modules;
        this.moduleStructure = props.moduleStructure;
        this.title = props.title;
        this.moduleButtons = props.moduleButtons;
    }

    render() {
        const minColumnWidth = 125;
        return (
            <Card>
                <CardTitle title={this.title}/>
                <div style={{overflowX: "auto"}}>
                    <div style={{minWidth: this.props.estimatedColumnWidth * this.moduleTitles.length}}>
                        {!this.props.verticalTable ?
                            <Table selectable={false}>
                                <TableHeader adjustForCheckbox={false} displaySelectAll={false} fixedHeader={true}>
                                    <TableRow>
                                        {this.moduleTitles.map(title => {
                                            return <TableHeaderColumn
                                                style={{minWidth: minColumnWidth}}>{title}</TableHeaderColumn>
                                        })}
                                    </TableRow>
                                </TableHeader>
                                <TableBody stripedRows={true} displayRowCheckbox={false}>
                                    {this.modules.map(module => {
                                            return [
                                                <TableRow>
                                                    {this.moduleStructure.map(column => {
                                                        if (typeof column == "string") {
                                                            const columnsSplit = column.split(".");
                                                            const key1 = columnsSplit[0];
                                                            const key2 = columnsSplit[1];
                                                            const key3 = columnsSplit[2];
                                                            //console.log(columnsSplit);
                                                            var value = module[key1];
                                                            if (module[key1][key2] != null) {
                                                                value = module[key1][key2];
                                                                if (module[key1][key2][key3] != null) value = module[key1][key2][key3];
                                                            }
                                                            return <TableRowColumn
                                                                style={{minWidth: minColumnWidth}}>{value.toString()}</TableRowColumn>
                                                        } else {
                                                            return <TableRowColumn
                                                                style={{minWidth: minColumnWidth}}>{column(module)}</TableRowColumn>
                                                        }
                                                    })}
                                                </TableRow>,
                                                (this.moduleButtons != null) ? <TableRow>
                                                        <div style={{width: "300%", marginTop: 4, marginLeft: 16}}>
                                                            {this.moduleButtons.map(moduleButton => {
                                                                return moduleButton(module);
                                                            })}
                                                        </div>
                                                    </TableRow> : null
                                            ]
                                        }
                                    )}
                                </TableBody>
                            </Table> :
                            <Table selectable={false}>
                                <TableBody displayRowCheckbox={false}>
                                {this.modules.map(module => {
                                    var index = 0;
                                    return [this.moduleStructure.map(column => {
                                                var title = this.moduleTitles[index++];

                                                if (typeof column == "string") {
                                                    const columnsSplit = column.split(".");
                                                    const key1 = columnsSplit[0];
                                                    const key2 = columnsSplit[1];
                                                    const key3 = columnsSplit[2];
                                                    var value = module[key1];
                                                    if (module[key1][key2] != null) {
                                                        value = module[key1][key2];
                                                        if (module[key1][key2][key3] != null) value = module[key1][key2][key3];
                                                    }
                                                    console.log(value);
                                                    return [
                                                        <TableRow>
                                                            <TableRowColumn
                                                                style={{color: "rgb(158, 158, 158)"}}>{title}</TableRowColumn>,
                                                            <TableRowColumn
                                                                style={{backgroundColor: "rgba(138, 220, 246, 0.4)"}}>{value}</TableRowColumn>
                                                        </TableRow>
                                                    ]
                                                } else {
                                                    return [
                                                        <TableRow>
                                                            <TableRowColumn
                                                                style={{color: "rgb(158, 158, 158)"}}>{title}</TableRowColumn>,
                                                            <TableRowColumn
                                                                style={{backgroundColor: "rgba(138, 220, 246, 0.4)"}}>{column(module)}</TableRowColumn>
                                                        </TableRow>
                                                    ]
                                                }
                                            }),
                                            (this.moduleButtons != null) ? <TableRow>
                                                    <div style={{width: "300%", marginTop: 4, marginLeft: 16}}>
                                                        {this.moduleButtons.map(moduleButton => {
                                                            return moduleButton(module);
                                                        })}
                                                    </div>
                                                </TableRow> : null
                                            ]
                                    })}
                                </TableBody>
                            </Table>
                        }
                    </div>
                </div>
            </Card>
        )
    }
}

ModuleTable.propTypes = {
    moduleTitles: React.PropTypes.array.isRequired,
    modules: React.PropTypes.array.isRequired,
    moduleStructure: React.PropTypes.array.isRequired,
    moduleButtons: React.PropTypes.array,
    title: React.PropTypes.string.isRequired,
    estimatedColumnWidth: React.PropTypes.number,
    verticalTable: React.PropTypes.boolean
};

ModuleTable.defaultProps = {
    estimatedColumnWidth: 106.6,
    verticalTable: false
};