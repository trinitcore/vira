/**
 * Created by Cormac on 30/07/2017.
 */
import React, {Component} from "react";
import {Card, CardActions, CardHeader, CardText} from "material-ui/Card";
import Avatar from "material-ui/Avatar";
import {Route, Link} from "react-router-dom";
import {Grid, Row, Col} from "react-bootstrap";

class TabCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            clicked: false,
            linkClicked: null
        };
    }

    render() {
        return (
            <Col sm = {6} md = {4} lg = {3}>
                <Link to = {this.props.url}>
                    <Card onClick = {() => this.props.linkClicked(this)} style = {{backgroundColor: this.state.clicked ? this.props.backgroundColor : "#fff"}}>
                        <CardHeader
                            avatar={
                                <Avatar color={this.props.color} backgroundColor={this.props.backgroundColor} icon={this.props.icon}/>
                            }
                            textStyle={{paddingRight: 0}}
                            title = {<h4>{this.props.title}</h4>}
                            titleColor={!this.state.clicked ? "#000" :"#fff"}
                            paddingRight = {0}
                        />
                    </Card>
                </Link>
            </Col>
        )
    }
}

export default TabCard;