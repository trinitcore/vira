/**
 * Created by Cormac on 23/09/2017.
 */
import React, {Component} from "react";
import {doSignOut, handleStaySignedIn} from "../layouts/public/Login";
import CircularProgress from "material-ui/CircularProgress";

class AuthenticationFrame extends Component {

    constructor(props) {
        super(props);
        this.state = {
            authenticated: false
        };

        this.needsAuthentication(true);
    }

    checkAndReAuthenticateIfNeeded(restData) {
        if (restData.loginValid == false) {
            doSignOut(true);
            this.needsAuthentication();
            return true;
        }
        return false;
    }

    needsAuthentication() {
        let context = this;
        context.setState({authenticated: false});
        handleStaySignedIn(this.props.parent, () => {
            context.state = {
                authenticated: true
            };
            context.setState({authenticated: true});
        });
    }

    render() {
        return (
            (this.state.authenticated) ? <div>{React.Children.map(this.props.children, child => {
                    return React.cloneElement(child,
                        {authenticationFrame: this}
                    )
            })}</div> : <div style={{width: "100%", textAlign: "center", top: "40%", position: "fixed"}}><CircularProgress/></div>
        );
    }
}

export default AuthenticationFrame;