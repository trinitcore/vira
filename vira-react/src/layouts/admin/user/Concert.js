import React, {Component} from "react";
import {Grid, Row, Col} from "react-bootstrap";
import {ModuleTable, ModuleTableButton} from "../../../components/ModuleTable";
import {Form, FormField, FormUploadField, FormButtonWrapper, FormButton, FormDatePicker} from "../../../components/Form";
import axios from "axios";

class Concert extends Component {

    constructor(props) {
        super(props);
        this.state = {
            didLoadData: false,
            concert: {}
        }
    }

    userID = this.props.match.params.userID;
    concertID = this.props.match.params.concertID;

    componentDidMount() {
        axios.get(link("admin.ConcertModuleServlet",this.concertID)).then(res => this.setState({didLoadData: true, concert: res.data.concert}))
    }

    render() {
        return (
            <div>
                {this.state.didLoadData ? <Grid fluid={true}>
                    <Row style={{marginBottom: 64}}>
                        <Form title={"Add Photo"} URL={link("admin.ConcertModuleServlet",this.concertID+"/addPhoto")}>
                            <FormField label={"Photo Name"} name={"photoName"}/>
                            <FormUploadField label={"Photo for upload"} name={"file"}/>

                            <FormButtonWrapper>
                                <FormButton xs={12} md={12} label={"Add"} submit={true}/>
                            </FormButtonWrapper>
                        </Form>

                        <Form title={"Adjust Concert Properties"} URL={link("admin.ConcertModuleServlet",this.concertID+"/adjustConcertProperties")}>
                            <FormField label={"Name"} name={"name"} defaultValue={this.state.concert.name}/>
                            <FormField label={"Location"} name={"location"} defaultValue={this.state.concert.location}/>

                            <FormDatePicker label={"Concert Date Time"} name={"dateTime"} defaultValue={this.state.concert.dateTime}/>

                            <FormButtonWrapper>
                                <FormButton xs={12} md={12} label={"Update"} submit={true}/>
                            </FormButtonWrapper>
                        </Form>
                    </Row>

                    <Row>
                         <ModuleTable
                            moduleTitles={["Photo Name","Concert ID","File Name"]}
                            modules={this.state.concert.photos}
                            moduleStructure={["name","concertID","fileName"]}
                            moduleButtons={[
                                (photo) => <ModuleTableButton label={"Open"} onTouchTap={() => window.open(uploadedResource(this.userID+"/"+this.concertID+"/"+photo.ID+"/"+photo.fileName))} primary={true}/>,
                                (photo) => <ModuleTableButton label={"Delete"} onTouchTap={() => axios.post(link("admin.ConcertModuleServlet",this.concertID+"/deletePhoto"),{photoID: photo.ID}).then(res => alert(res.data.message))} secondary={true}/>,
                                (photo) => <ModuleTableButton label={"Set as cover photo"} onTouchTap={() => axios.post(link("admin.ConcertModuleServlet",this.concertID+"/setCoverPhoto"),{photoID: photo.ID, photoFileName: photo.fileName}).then(res => alert(res.data.message))}/>
                            ]}
                            title={"Photos"}/>
                    </Row>
                </Grid> : null }
            </div>
        )
    }

}

export default Concert;