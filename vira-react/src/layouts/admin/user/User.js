import React, {Component} from "react";
import {Grid, Row, Col} from "react-bootstrap";
import {ModuleTable, ModuleTableButton} from "../../../components/ModuleTable";
import axios from "axios";
import {Form, FormField, FormButton, FormButtonWrapper, FormLinkWrapText, FormCheckbox, FormDatePicker} from "../../../components/Form";

class User extends Component {

    constructor(props) {
        super(props);
        this.state = {
            concerts: [],
            dataDidLoad: false
        }
    }

    componentDidMount() {
        axios.get(link("admin.ConcertModuleServlet","?userID="+this.props.match.params.userID)).then(res => this.setState({concerts: res.data.concerts, dataDidLoad: true}))
    }

    render() {
        return (
            <div>
                <Grid fluid={true}>
                    <Row style={{marginBottom: 64}}>
                        <Form additionalParameters={{clientID:parseInt(this.props.match.params.userID)}} title={"Add Concert"} URL={link("admin.ConcertModuleServlet","create")}>
                            <FormField label={"Name"} name={"name"}/>
                            <FormDatePicker label={"Concert Date Time"} name={"concertDateTime"}/>
                            <FormField label={"Location"} name={"location"}/>

                            <FormButtonWrapper>
                                <FormButton xs={12} md={12} label={"Add"} submit={true}/>
                            </FormButtonWrapper>
                        </Form>
                    </Row>
                    <Row>
                        {this.state.dataDidLoad ? <ModuleTable
                            moduleTitles={["Name","Date Time","Date Time Creation","Location","Cover Photo"]}
                            modules={this.state.concerts}
                            moduleStructure={["name","formattedDateTime","formattedCreationDateTime","location","coverPhoto"]}
                            moduleButtons={[
                                (concert) => <ModuleTableButton label={"Open"} primary={true} onTouchTap={() => this.props.history.push("/admin/dashboard/user/"+this.props.match.params.userID+"/concert/"+concert.ID)}/>
                            ]}
                            title={"Concerts"}/> : null}
                    </Row>
                </Grid>
            </div>
        )
    }

}

export default User;