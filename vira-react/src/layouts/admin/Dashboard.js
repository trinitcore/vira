import React, {Component} from "react";
import {Grid, Row, Col} from "react-bootstrap";
import AdminDrawer from "./AdminDrawer"
import {ModuleTable, ModuleTableButton} from "../../components/ModuleTable"
import axios from "axios"

class Dashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    componentDidMount() {
        axios.get(link("admin.AdminDashboardREST","allClients")).then(res => { if(res.data.ok === true) this.setState({data: res.data.users})} )
    }

    render() {
        return (
            <div className={"Container"}>
                <AdminDrawer main = {this.props.main}/>
                <h4>Welcome David</h4>

                {(this.state.data.length !== 0) ?
                    <ModuleTable
                        moduleButtons={[(user) => <ModuleTableButton label="Open" primary={true} onTouchTap={() => this.props.history.push("/admin/dashboard/user/"+user.ID)}/>]}
                        moduleTitles={["Firstname","Lastname","Email"]}
                        modules={this.state.data}
                        moduleStructure={["firstname","lastname","email"]}
                        title={"Client Users"}/> : null}
            </div>
        )
    }

}

export default Dashboard;