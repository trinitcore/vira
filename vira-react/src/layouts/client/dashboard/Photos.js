import React, {Component} from "react";
import {Grid, Row, Col} from "react-bootstrap";
import FlatButton from "material-ui/FlatButton"
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import axios from "axios"
import {userAttribute} from "../../../Main"
import ClientDrawer from "../ClientDrawer"
import "./Photo.css"

class Photos extends Component {

    constructor(props) {
        super(props);
        this.state = {
            concert: {},
            didLoadData: false
        }
    }

    componentDidMount() {
        axios.get(link("client.ConcertModuleServlet",this.props.match.params.concertID)).then(res => { if (res.data.ok === true) this.setState({concert: res.data.concert, didLoadData: true}) })
    }

    render() {
        return (
            <div>
                <ClientDrawer main={this.props.main}/>
                {this.state.didLoadData ? <div style={{paddingLeft: 32, paddingRight: 32, paddingTop: 32}} className={"Container"}>
                    <Grid>
                        <Row>
                            <Col md={6}>
                                <Card style={{maxHeight: 400, width: "100%"}}>
                                    <CardMedia style={{padding: 16}}>
                                        <img src={uploadedResource(userAttribute("ID")+"/"+this.state.concert.ID+"/"+this.state.concert.coverPhotoID+"/"+this.state.concert.coverPhoto)} />
                                    </CardMedia>
                                </Card>
                            </Col>
                            <Col md={6}>
                                <Row style={{marginLeft:16, marginTop: 100}}>
                                    <Col xs={12}>
                                        <h1><strong>{this.state.concert.name}</strong></h1>
                                    </Col>
                                    <Col xs={12}>
                                        <h5>{this.state.concert.location}</h5>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Grid>


                    <Grid fluid={true} style={{marginTop: 48}}>
                            <div style={{textAlign: "center", marginBottom: 32}}>
                                <h3>Concert Night Photos</h3>
                            </div>
                        <Row>
                            {this.state.concert.photos.map(photo => {
                                const imageLink = uploadedResource(userAttribute("ID") + "/" + this.state.concert.ID + "/" + photo.ID + "/" + photo.fileName)
                                    return <Col md={2} style={{marginBottom:16}}>
                                        <Card>
                                            <CardMedia style={{padding: 16, paddingBottom: 0, height: "100%"}}>
                                                <img style={{height: "160px"}}
                                                     src={imageLink}/>
                                            </CardMedia>
                                            <CardTitle title={photo.name} subtitle="12/7/2018"/>
                                            <CardActions style={{marginTop: -20, position: "relative", bottom: 0}}>
                                                <FlatButton label="Download"
                                                            onClick={() => window.open(imageLink)}/>
                                                <FlatButton label="View"/>
                                            </CardActions>
                                        </Card>
                                    </Col>
                                }
                            )}
                        </Row>
                    </Grid>
                </div> : null }
            </div>
        )
    }

}

export default Photos;