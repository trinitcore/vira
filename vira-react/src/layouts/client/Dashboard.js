import React, {Component} from "react";
import {Grid, Row, Col} from "react-bootstrap";
import ClientDrawer from "./ClientDrawer"
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import {LineChart, Line, AreaChart, Area, Brush, XAxis, YAxis, CartesianGrid, Tooltip} from "recharts"
import axios from "axios";

class Dashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            concerts: []
        };
    }

    componentDidMount() {
        axios.get(link("client.ConcertModuleServlet","")).then(res => {if (res.data.ok === true) this.setState({concerts: res.data.concerts})})
    }

    render() {
        const data = [
            {name: 'Day 1', Impressions: 4000},
            {name: 'Page B', Impressions: 7222},
            {name: 'Page C', Impressions: 6222},
            {name: 'Page D', Impressions: 5400},
            {name: 'Page E', Impressions: 3200},
            {name: 'Page F', Impressions: 2500},
            {name: 'Page G', Impressions: 1209},
        ];

        return (
            <div className={"Container"}>
                <ClientDrawer main = {this.props.main}/>
                <h4>Welcome Cormac</h4>
                <Grid fluid={true} style={{marginTop:32}}>
                    <Row>
                        {this.state.concerts.map(concert =>
                            <Col sm={2} style={{width : 332}}>
                                <Card style={{width:322}}>
                                    <CardMedia>
                                        <img height="215" src={uploadedResource(concert.userID+"/"+concert.ID+"/"+concert.coverPhotoID+"/"+concert.coverPhoto)}/>
                                    </CardMedia>

                                    <h3 style={{textAlign:"center"}}><strong>{concert.name}</strong></h3>
                                    <h5 style={{textAlign:"center"}}>{concert.location}</h5>

                                    <div>
                                        <AreaChart width={323} height={130} data={data}
                                                   margin={{top: 0, right: 0, left: 0, bottom: 0}}>
                                            <Tooltip labelFormatter={(data) => "Day "+data}/>
                                            <Area type='monotone' dataKey='Impressions' stroke='#842896' fill='#842896' />
                                        </AreaChart>
                                    </div>
                                    <div style={{width: 323,backgroundColor: "#525560",height: 55, padding:10, textAlign: "center"}}>
                                        <RaisedButton label={"View Photos"} onClick={() => this.props.history.push("/client/dashboard/"+concert.ID)} primary={true}/>
                                    </div>
                                </Card>
                            </Col>
                        )}
                    </Row>
                </Grid>
            </div>
        )
    }

}

export default Dashboard;