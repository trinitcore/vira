/**
 * Created by Cormac on 31/07/2017.
 */
import React, {Component} from "react";
import {Grid, Row, Col} from "react-bootstrap";
import {Card, CardActions, CardHeader, CardText} from "material-ui/Card";
import {Form, FormField, FormButton, FormButtonWrapper, FormLinkWrapText, FormCheckbox} from "../../components/Form";
import axios from "axios";
import {Route, Link, HashRouter} from "react-router-dom";
import qs from "qs";
import PublicDrawer from "./PublicDrawer";
import {setCookie, cookies, userData, removeCookie, setCookieWithAge} from "../../Main";
import AuthenticationFrame from "../../components/AuthenticationFrame";

export const redirectLinks = {
    "bookAppointment" : {
        url : "/client/dashboard/book"
    },
    "callSession" : {
        url : (parameters) => "/call/" + parameters.appointmentID + "/" + parameters.assignAuthKey,
        additionalHTML : (html) => {
            html(<div style={{textAlign: "center"}}>
                <h3>You are about to join a call</h3>
            </div>)
        }
    }
};

export const ajaxLinks = {
    "assignAppointment" : {
        url : link("adviser.Appointment","acceptAppointment"),
        parameters: ["appointmentID", "assignAuthKey"],
        additionalHTML: (html, parameters) => {
            axios.get("/rest/public/appointment?ID="+parameters.appointmentID + "&assignAuthKey="+parameters.assignAuthKey).then(res => {
                const data = res.data;
                html(<div style={{textAlign: "center"}}>
                    <h3>You are accepting an appointment</h3>
                    <p>For {data.date} @  {data.time}<br />Sign in to <strong>confirm</strong></p>
                </div>)
            });
        }
    },
    "confirmUser" : {
        additionalHTML: (html, parameters) => {
            axios.get(link("auth.UserREST","confirmAccount?confirmationKey="+parameters.confirmationKey)).then(res => {
                const data = res.data;
                html(
                    <div style={{textAlign: "center"}}>
                        <h3>{data.message}</h3>
                    </div>
                )
            })
        }
    }
};

export function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

export function doSignOut(staySignedIn) {
    removeCookie("isLoggedIn");
    removeCookie("userData");
    if (staySignedIn != true) {
        removeCookie("staySignedIn");
        removeCookie("email");
        removeCookie("password");
    }
    console.log(cookies().getAll());
}

export function handleStaySignedIn(context, completion) {
    console.log(cookies().getAll());
    if (cookies().get("isLoggedIn") == "true") {
        var data = userData();
        handleRedirect(data.userType, context);
        completion(true);
    }
    else if (cookies().get("staySignedIn") == "true") {
        doSignIn(
            cookies().get("email"),
            cookies().get("password"),
            true,
            (success, r) => {
                if (success) {
                    if (context.props.path == "/login") {
                        handleRedirect(r.userType, context);
                    } else {
                        completion(true);
                    }
                } else {
                    setCookie("staySignIn", false);
                    handleRedirect(null, context);
                    completion(false);
                }
            }
        );
    } else {
        handleRedirect(null, context);
        completion(false);
    }
}

export function doSignIn(email, password, staySignedIn, completion) {
    console.log("Do sign in");
    axios.post(link("auth.UserREST","login"),
        qs.stringify({
            enableRedirect:false,
            email: email,
            password: password
        })).then(res => {
        const data = res.data;
        console.log(data);
        if (data.ok) {
            data.userData.appointments = null;
            setCookie("userData", data.userData);
            setCookie("isLoggedIn", true);

            let thirtyDays = 2592000;
            setCookieWithAge("staySignedIn", staySignedIn, thirtyDays);
            if (staySignedIn) {
                setCookieWithAge("email", email, thirtyDays);
                setCookieWithAge("password", password, thirtyDays);
            }
            completion(true, data.userData);
        } else {
            completion(false, data.message);
        }
    });
}

const allowedPages = {
    "0" : [
        "/client/dashboard",
        "/call",
        "/message",
        "/client/pay/"
    ],
    "1" : [
        "/adviser/dashboard",
        "/message",
        "/call"
    ],
    "-1" : [
        "/admin/dashboard",
        "/admin/user"
    ]
};

function isOnAllowedPage(userType, path) {
    const userTypePages = allowedPages[userType];
    for (var i = 0; i < userTypePages.length; i++) {
        const page = userTypePages[i];
        if (path.indexOf(page) >= 0) {
            return true;
        }
    }
    return false;
}

export function handleRedirect(userType, context) {
    if (userType == null) {
        if (!context.props.path.indexOf("/login") >= 0) {
            context.props.history.push('/login');
        }
    } else {
        if (userType == 0) {
            if (!isOnAllowedPage("0", context.props.path)) context.props.history.push('/client/dashboard');
        } else if (userType == 1) {
            if (!isOnAllowedPage("1", context.props.path)) context.props.history.push('/adviser/dashboard');
        } else if (userType == -1) {
            if (!isOnAllowedPage("-1", context.props.path)) context.props.history.push('/admin/dashboard');
        }
    }
}

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            inProgress: false,
            incorrectCredentials: false,
            additionalHTML: null
        };
    }

    formatRedirectLink(redirectData) {
        var parameters = {};
        if (redirectData.parameters != null) {
            for (var i = 0; i < redirectData.parameters.length; i++) {
                const parameter = redirectData.parameters[i];
                parameters[parameter] = this.props.match.params[parameter];
            }
        }

        var url;
        if (typeof redirectData.url == "string") url = redirectData.url;
        else url = redirectData.url(this.props.match.params);

        const data = url + "?" + qs.stringify(parameters);
        return data;
    }

    handleSignIn = () =>  doSignIn(this.email.getValue(), this.password.getValue(), this);

    redirect = redirectLinks[this.props.match.params.redirectPage];
    ajax = ajaxLinks[this.props.match.params.redirectPage];

    componentDidMount() {
        var additionalHandling = null;
        if (this.ajax != null) additionalHandling = this.ajax;
        else if (this.redirect != null) additionalHandling = this.redirect;

        if (additionalHandling != null) {
            if (additionalHandling.additionalHTML != null) {
                const htmlTrigger = (givenHTML) => this.setState({additionalHTML: givenHTML});
                additionalHandling.additionalHTML(htmlTrigger, this.props.match.params)
            }
        }
    }

    render() {
        const body =
            <div>
                <PublicDrawer main = {this.props.main}/>
                <div id="additionalHTML">
                    {this.state.additionalHTML}
                </div>
                <Form backButton = {true} center={true} parent = {this} postRequestHideButtonWrapper = {true} title = "Sign In" onFormSubmit = {(fields, completion) =>
                    doSignIn(fields.email, fields.password, fields.staySignedIn, (success, data) => {
                        if (success) {
                            if (this.redirect != null) {
                                this.props.history.push(this.formatRedirectLink(this.redirect));
                            } else if (this.ajax != null && this.ajax.url != null) {
                                axios.get(this.formatRedirectLink(this.ajax)).then(res => {
                                    completion(res.data.success ? "green" : "red", res.data.message)
                                });
                            }
                            else {
                                handleRedirect(data.userType, this);
                            }
                        } else {
                            completion("red", data);
                        }
                    })
                }>
                    <FormField name="email" type="email" label="Email"/>
                    <FormField name="password" type="password" label="Password"/>
                    <FormCheckbox name="staySignedIn" label="Stay Signed In"/>
                    <FormButtonWrapper>
                        <FormButton xs={12} md={12} label="Sign In"/>
                    </FormButtonWrapper>
                    <FormLinkWrapText textWrap="Don't have an account?" linkText="Sign Up" linkTo="/signUp"/>
                    <FormLinkWrapText textWrap="Did you" linkText="forget your password?" linkTo="/forgotPassword"/>
                </Form>
            </div>;
        return (
            (this.redirect != null || this.ajax != null) ? body :
                <AuthenticationFrame parent = {this}>
                    {body}
                </AuthenticationFrame>
        );
    }
}

export default Login;