import React, {Component} from "react";
import {Grid, Row, Col} from "react-bootstrap";
import PublicDrawer from "./PublicDrawer";

class Home extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                <PublicDrawer main = {this.props.main} />
                <h1>Home Page</h1>
            </div>
        )
    }

}

export default Home;