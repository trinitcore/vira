import React, {Component} from "react";
import {Grid, Row, Col} from "react-bootstrap";
import {Card, CardActions, CardHeader, CardText} from "material-ui/Card";
import {Route, Link, HashRouter} from "react-router-dom";
import MenuItem from "material-ui/MenuItem";
import Drawer from "material-ui/Drawer"

class PublicDrawer extends Component {

    constructor(props) {
        super(props);
        this.props.main.setState({
            open: false
        });
        this.props.main.setAppBarButtonHidden(false);
    }

    handleClick = () => this.props.main.appBarButtonTap(false);

    render() {
        return (
            <Drawer width={200} onRequestChange={this.props.main.appBarButtonTap}
                    openSecondary={true} open={this.props.main.state.open} docked={false}>
                <Link to="/"><MenuItem onClick={this.handleClick}>Home</MenuItem></Link>
                <Link to="/login"><MenuItem onClick={this.handleClick}>Sign In</MenuItem></Link>
                <Link to="/signUp"><MenuItem onClick={this.handleClick}>Sign Up</MenuItem></Link>
            </Drawer>
        )
    }
}

export default PublicDrawer;