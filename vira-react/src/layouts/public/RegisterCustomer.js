/**
 * Created by Cormac on 31/07/2017.
 */
import React, {Component} from "react";
import {redirectLinks} from "./Login";
import {
    Form,
    FormField,
    FormButton,
    FormButtonWrapper,
    FormLinkWrapText
} from "../../components/Form.js";
import {Route, Link, HashRouter, browserHistory} from "react-router-dom";
import PublicDrawer from "./PublicDrawer";
import {doSignIn, handleRedirect} from "./Login";

class RegisterCustomer extends Component {

    constructor(props) {
        super(props);
    }

    redirect = redirectLinks[this.props.match.params.redirectPage];

    render() {
        return (
            <div>
                <PublicDrawer main = {this.props.main}/>
                {(this.props.match.params.redirectPage == "bookAppointment") ?
                    <div style={{textAlign: "center"}}>
                        <h2>Getting started</h2>
                        <h6>Before you make an appointment, you'll need to create an account first</h6>
                    </div> : null}

                <Form backButton = {true} center={true} parent = {this} title = {"Customer Sign Up"} URL={(link("auth.UserREST","signUp"))}
                additionalParameters={{isRegister: true, userType: 0}} postRequestHandler = {(success, message, buttonWrapper, formParameters) => {
                    if (success) {
                        doSignIn(formParameters.field[2].getValue(), formParameters.field[3].getValue(), false, (success, data) => {
                            if (this.redirect != null) {
                                this.props.history.push(this.redirect.url);
                            } else {
                                handleRedirect(data.userType, this);
                            }
                        });
                        return false;
                    }
                    return true;
                }}>
                    <FormField name="firstname" label="First Name"/>
                    <FormField name="lastname" label="Last Name"/>
                    <FormField name="email" label = {"Email"} />
                    <FormField type="password" name="password" label = {"Password"} />
                    <FormButtonWrapper>
                        <FormButton xs={12} md={12} label="Sign Up"/>
                    </FormButtonWrapper>
                    <FormLinkWrapText textWrap="Already with us?" linkText="Sign In" linkTo="/login"/>
                </Form>
            </div>
        )
    }
}

export default RegisterCustomer;