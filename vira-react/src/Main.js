import React, {Component} from "react";
import {BrowserRouter as Router, Route, Link, HashRouter} from "react-router-dom";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import AppBar from "material-ui/AppBar";
import getMuiTheme from "material-ui/styles/getMuiTheme";
import {PropsRoute, AuthenticationPropsRoute} from "./components/PropsRoute";
import Cookies from "universal-cookie";
import Menu from "material-ui/svg-icons/navigation/menu";
import IconButton from "material-ui/IconButton";
import "./App.css"

import Home from "./layouts/public/Home"
import Login from "./layouts/public/Login"
import RegisterCustomer from "./layouts/public/RegisterCustomer"
import ClientDashboard from "./layouts/client/Dashboard"
import AdminDashboard from "./layouts/admin/Dashboard"
import AdminUser from "./layouts/admin/user/User"
import AdminConcert from "./layouts/admin/user/Concert"
import Photos from "./layouts/client/dashboard/Photos"

export function cookies() {
    return new Cookies();
}

export function userData() {
    return cookies().get("userData");
}

export function userAttribute(key) {
    if (cookies().get("userData") != null) {
        return cookies().get("userData")[key];
    }
    return null;
}

export function removeCookie(key) {
    cookies().remove(key, {path: '/'});
}

export function setCookie(key, value) {
    cookies().set(key, value, {path: '/'});
}

export function setCookieWithAge(key, value, age) {
    cookies().set(key, value, {path: '/', maxAge: age});
}

const font = "'Raleway', sans-serif";

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: "#f778c5"
    },
    fontFamily: font
});

export function handleNotLoggedIn(component) {
    console.log(component);
    component.props.history.push('/login?redirectPage=' + component.props.match.url);
}

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            appBarButtonHidden: true,
            appBarTitleHomeTapDisable: false,
            footerHidden: false,
            appBarHidden: false,
            scrollingDisable: false
        };
    }

    handleToggle = () => this.setState({open: !this.state.open});

    handleClose = () => this.setState({open: false});

    backLink = "/";

    setAppBarButtonHidden = (hidden) => {
        this.setState({appBarButtonHidden: hidden});
    };

    setAppBarTitleHomeTapDisable = (disable) => {
        this.setState({appBarTitleHomeTapDisable: disable});
    };

    setFooterHidden = (disable) => {
        this.setState({footerHidden: disable});
    };

    setAppBarHidden = (disable) => {
        this.setState({appBarHidden: disable});
    };

    setScrollDisabled = (disable) => {
        this.setState({scrollingDisable: disable});
    };

    appBarButtonTap = (open) => {
        console.log("Open drawer.");
        this.setState({open: open});
    };

    render() {
        return (
            <Router>
                <MuiThemeProvider muiTheme={muiTheme}>
                    <div>
                        {this.state.appBarHidden ? null :
                            <AppBar
                                style={{backgroundColor: "#FFF", position: "fixed"}}
                                title={
                                    <div style = {{position:"relative", left: 0}}><img style={{height: 33}} src = {resource("Logo.png")} /></div>
                                }
                                iconElementRight={
                                    this.state.appBarButtonHidden ? null : <IconButton onClick={() => {
                                        this.appBarButtonTap(true);
                                    }}><Menu color="#15b9ed"/></IconButton>
                                }
                            />}

                        <div className="Container" style={{paddingTop: 75}}>
                            <div className="propContents">
                                <PropsRoute exact path="/admin/dashboard" main = {this} component = {AdminDashboard} />
                                <PropsRoute exact path="/admin/dashboard/user/:userID" main = {this} component = {AdminUser} />
                                <PropsRoute exact path="/admin/dashboard/user/:userID/concert/:concertID" main = {this} component = {AdminConcert} />

                                <PropsRoute exact path="/client/dashboard" main = {this} component = {ClientDashboard} />
                                <PropsRoute exact path="/client/dashboard/:concertID" main = {this} component = {Photos} />

                                <PropsRoute exact path="/login" main = {this} component = {Login} />
                                <PropsRoute exact path="/signUp" main = {this} component = {RegisterCustomer} />
                                <PropsRoute exact path="/" main = {this} component = {Home} />
                            </div>
                        </div>
                    </div>

                </MuiThemeProvider>
            </Router>
        );
    }
}

export default Main;
