package com.trinitcore.vira.html

import com.trinitcore.asd.servlet.UploadedWebResourcesServlet
import com.trinitcore.asd.user.User
import com.trinitcore.vira.users.admin.Admin
import javax.servlet.annotation.WebServlet

@WebServlet("/uploadedResources/*")
class MainUploadedWebResources : UploadedWebResourcesServlet() {
    override fun verify(user: User, path: List<String>): Boolean {
        return user.getID().toString() == path[0] || user is Admin
    }

}