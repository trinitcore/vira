package com.trinitcore.vira.html

import com.trinitcore.asd.responseTools.status.html.subNodes.ScriptNode
import com.trinitcore.asd.responseTools.status.html.subNodes.StylesheetNode
import com.trinitcore.asd.servlet.ReactJSServlet
import com.trinitcore.asd.servlet.UploadedWebResourcesServlet
import com.trinitcore.asd.servlet.WebResourcesServlet
import javax.servlet.annotation.WebServlet
import kotlin.reflect.KClass

@WebServlet("/*")
class MainReactJS : ReactJSServlet() {
    override fun uploadedResourceServlet(): KClass<out UploadedWebResourcesServlet> = MainUploadedWebResources::class

    override fun additionalScripts(): Array<ScriptNode> = arrayOf(
    )

    override fun additionalStylesheets(): Array<StylesheetNode> = arrayOf(
            StylesheetNode("styles/bootstrap.min.css"),
            StylesheetNode("https://fonts.googleapis.com/css?family=Raleway")
    )

    override fun title(): String = "Virra"

    override fun webResourceServlet(): KClass<out WebResourcesServlet> = MainWebResources::class
}