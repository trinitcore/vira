package com.trinitcore.vira.html

import com.trinitcore.asd.servlet.WebResourcesServlet
import javax.servlet.annotation.WebServlet

@WebServlet("/webResources/*")
class MainWebResources : WebResourcesServlet() {
    override fun subWebResourcePath(): String = "main"
}