package com.trinitcore.vira.modules

import com.trinitcore.sqlv2.queryUtils.module.Attribute
import com.trinitcore.sqlv2.queryUtils.module.DataModule

class Photo : DataModule() {

    var name: String by Attribute()
    var fileName: String by Attribute()
    var concertID: Int by Attribute()

}