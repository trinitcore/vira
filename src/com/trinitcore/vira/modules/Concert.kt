package com.trinitcore.vira.modules

import com.trinitcore.asd.annotations.SubWebServlet
import com.trinitcore.asd.annotations.Web
import com.trinitcore.asd.requestTools.UploadedFile
import com.trinitcore.asd.responseTools.format.nArray
import com.trinitcore.asd.responseTools.format.nObject
import com.trinitcore.asd.responseTools.status.StatusResult
import com.trinitcore.asd.responseTools.status.json.OKFormStatus
import com.trinitcore.asd.responseTools.status.json.OKStatus
import com.trinitcore.sqlv2.commonUtils.QMap
import com.trinitcore.sqlv2.queryUtils.module.*
import com.trinitcore.sqlv2.queryUtils.parameters.Where
import org.json.simple.JSONArray
import org.json.simple.JSONObject
import java.text.SimpleDateFormat

class Concert : DataModule() {

    var name: String by Attribute()
    var location: String by Attribute()
    var dateTime: Long by Attribute()
    var formattedDateTime: String by ReformableAttribute()
    var formattedCreationDateTime: String by ReformableAttribute()
    var userID: Int by Attribute()
    var dateTimeCreation: Long by Attribute()

    var coverPhoto: String? by NullableAttribute()
    var coverPhotoID: Int? by NullableAttribute()

    @SubWebServlet
    var photos: DataModules<Photo> by Attribute()

    @Web
    fun addPhoto(file: UploadedFile, photoName: String) : StatusResult {

        val photoID = photos.insert(
                QMap("name",photoName),
                QMap("concertID",getID()),
                QMap("fileName", file.fileName!!)
        )!!.getID()

        file.saveFile(userID.toString() + "/" + getID() + "/" + photoID)
        return OKFormStatus("Photo added successfully.")
    }

    @Web fun deletePhoto(photoID: Int) : StatusResult {
        photos.deleteRow(Where().value("ID",photoID))
        return OKFormStatus("Deleted photo successfully.")
    }

    @Web fun adjustConcertProperties(name: String, location: String, dateTime: String) : StatusResult {
        this.name = name
        this.location = location
        this.dateTime = SimpleDateFormat("dd/MM/yyyy").parse(dateTime).time
        return OKFormStatus("Concert properties adjusted successfully.")
    }

    @Web fun setCoverPhoto(photoID: Int, photoFileName: String) : StatusResult {
        this.coverPhotoID = photoID
        this.coverPhoto = photoFileName
        return OKFormStatus("Photo set as cover successfully.")
    }

    @Web fun default() : StatusResult = OKStatus("concert", mapOf(
                "ID" to getID(),
                "name" to name,
                "location" to location,
                "formattedDateTime" to formattedDateTime,
                "formattedCreationDateTime" to formattedCreationDateTime,
                "dateTime" to dateTime,
                "userID" to userID,
                "dateTimeCreation" to dateTimeCreation,

                "coverPhoto" to (coverPhoto ?: "None"),
                "coverPhotoID" to (coverPhotoID ?: "None"),

                "photos" to photos.toJSONArray()
        )
    )

}