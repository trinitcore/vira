package com.trinitcore.vira.rest.admin

import com.trinitcore.asd.annotations.Web
import com.trinitcore.asd.responseTools.status.StatusResult
import com.trinitcore.asd.responseTools.status.json.OKFormStatus
import com.trinitcore.asd.responseTools.status.json.OKStatus
import com.trinitcore.asd.servlet.RESTServlet
import com.trinitcore.sqlv2.queryObjects.Table
import com.trinitcore.sqlv2.queryUtils.parameters.Where
import javax.servlet.annotation.WebServlet

@WebServlet("/admin/REST/dashboard/*")
class AdminDashboardREST : RESTServlet() {

    @Web
    fun allClients(users: Table) = OKStatus("users",users.find(Where().value("userType",0)).let {
        var a = it.toJSONArray()
        return@let a
    })

}