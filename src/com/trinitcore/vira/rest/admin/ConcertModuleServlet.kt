package com.trinitcore.vira.rest.admin

import com.trinitcore.asd.annotations.Web
import com.trinitcore.asd.responseTools.status.StatusResult
import com.trinitcore.asd.responseTools.status.json.OKFormStatus
import com.trinitcore.asd.responseTools.status.json.OKStatus
import com.trinitcore.sqlv2.commonUtils.QMap
import com.trinitcore.sqlv2.queryUtils.parameters.Where
import com.trinitcore.vira.users.admin.Admin
import com.trinitcore.vira.users.client.Client
import java.text.SimpleDateFormat
import java.util.*
import javax.servlet.annotation.MultipartConfig
import javax.servlet.annotation.WebServlet

@MultipartConfig
@WebServlet("/admin/REST/concert/*")
class ConcertModuleServlet : com.trinitcore.vira.rest.clientAdmin.ConcertModuleServlet() {

    @Web
    fun create(clientID: Int, name: String, concertDateTime: String, location: String) : StatusResult {
        concert.insert(QMap("name",name), QMap("dateTime", SimpleDateFormat("dd/MM/yyyy").parse(concertDateTime).time), QMap("userID", clientID), QMap("dateTimeCreation", Date().time), QMap("location",location))
        return OKFormStatus("Concert added successfully.")
    }

    @Web
    fun default(userID: Int) : StatusResult {
        val concerts = concert.find(Where().value("userID",userID))
        return OKStatus("concerts", concerts)
    }

}