package com.trinitcore.vira.rest.clientAdmin

import com.trinitcore.asd.annotations.Web
import com.trinitcore.asd.requestTools.Parameters
import com.trinitcore.asd.requestTools.SessionAttributes
import com.trinitcore.asd.responseTools.status.StatusResult
import com.trinitcore.asd.responseTools.status.json.OKStatus
import com.trinitcore.asd.servlet.ModuleServlet
import com.trinitcore.sqlv2.commonUtils.QMap
import com.trinitcore.sqlv2.queryObjects.ModuleTable
import com.trinitcore.sqlv2.queryUtils.module.DataModule
import com.trinitcore.vira.modules.Concert
import com.trinitcore.vira.users.client.Client
import java.util.*
import javax.servlet.annotation.WebServlet

open class ConcertModuleServlet : ModuleServlet() {
    override val pathStructure: Array<String>
        get() = arrayOf("ID")

    lateinit var concert: ModuleTable<Concert>

    override fun moduleCreation(pathParameters: Parameters, sessionAttributes: SessionAttributes<*>): DataModule {
        return concert.findModuleByID(pathParameters["ID"].toString().toInt())!!.let {
            return@let it
        }
    }

}