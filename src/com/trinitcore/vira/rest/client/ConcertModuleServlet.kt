package com.trinitcore.vira.rest.client

import com.trinitcore.asd.annotations.Web
import com.trinitcore.asd.responseTools.status.StatusResult
import com.trinitcore.asd.responseTools.status.json.OKStatus
import com.trinitcore.sqlv2.queryUtils.parameters.Where
import com.trinitcore.vira.users.client.Client
import javax.servlet.annotation.WebServlet

@WebServlet("/client/REST/concert/*")
class ConcertModuleServlet : com.trinitcore.vira.rest.clientAdmin.ConcertModuleServlet() {

    @Web
    fun default(user: Client) : StatusResult {
        return OKStatus("concerts", concert.find(Where().value("userID",user.getID()))
                .let {
                    val a = it.toJSONArray()
                    return@let it
                }.toJSONArray())
    }

}