package com.trinitcore.vira.users.admin

import com.trinitcore.asd.resourceTools.email.contents.EmailContent
import com.trinitcore.asd.servlet.AdvancedServlet
import com.trinitcore.asd.user.User
import com.trinitcore.asd.user.UserType
import com.trinitcore.sqlv2.queryObjects.builders.TableBuilder
import com.trinitcore.vira.rest.admin.AdminDashboardREST
import com.trinitcore.vira.rest.admin.ConcertModuleServlet
import kotlin.reflect.KClass

class AdminUserType() : UserType<Admin>(AdminDashboardREST::class, ConcertModuleServlet::class, ID = -1) {
    override fun configureUserTable(userTableBuilder: TableBuilder) {

    }

    override fun confirmationEmailContent(userData: User): EmailContent? = null

    override fun tables(): Array<TableBuilder> = arrayOf(

    )
}