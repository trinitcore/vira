package com.trinitcore.vira.users.admin

import com.trinitcore.asd.user.User
import com.trinitcore.asd.user.UserType
import com.trinitcore.vira.users.GenericUser

class Admin(userType: UserType<*>) : GenericUser(userType) {
}