package com.trinitcore.vira.users

import com.trinitcore.asd.resourceTools.email.contents.EmailContent
import com.trinitcore.asd.user.PublicUserType
import com.trinitcore.asd.user.User
import com.trinitcore.sqlv2.queryObjects.builders.TableBuilder
import com.trinitcore.vira.html.MainReactJS
import com.trinitcore.vira.html.MainUploadedWebResources
import com.trinitcore.vira.html.MainWebResources

class PublicType : PublicUserType(MainReactJS::class, MainWebResources::class, MainUploadedWebResources::class) {

    override fun publicTables(): Array<TableBuilder> = arrayOf()

}