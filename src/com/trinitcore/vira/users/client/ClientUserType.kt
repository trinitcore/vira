package com.trinitcore.vira.users.client

import com.trinitcore.asd.resourceTools.email.contents.EmailContent
import com.trinitcore.asd.user.User
import com.trinitcore.asd.user.UserType
import com.trinitcore.asd.user.configurations.Column
import com.trinitcore.asd.user.configurations.ColumnType
import com.trinitcore.sqlv2.queryObjects.builders.ModuleTableBuilder
import com.trinitcore.sqlv2.queryObjects.builders.TableBuilder
import com.trinitcore.sqlv2.queryUtils.associationV2.Associating
import com.trinitcore.sqlv2.queryUtils.associationV2.format.DateFormatAssociation
import com.trinitcore.sqlv2.queryUtils.associationV2.table.RowsAssociation
import com.trinitcore.vira.modules.Concert
import com.trinitcore.vira.modules.Photo
import com.trinitcore.vira.rest.client.ConcertModuleServlet

class ClientUserType() : UserType<Client>(ConcertModuleServlet::class, ID = 0) {
    override fun configureUserTable(userTableBuilder: TableBuilder) {

    }

    override fun confirmationEmailContent(userData: User): EmailContent? = null

    override fun userColumns(): Array<ColumnType> = arrayOf(
            Column("firstname"),
            Column("lastname")
    )

    override fun tables(): Array<TableBuilder> = arrayOf(

    )
}