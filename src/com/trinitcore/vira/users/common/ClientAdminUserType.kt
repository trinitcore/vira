package com.trinitcore.vira.users.common

import com.trinitcore.asd.user.CommonUserType
import com.trinitcore.asd.user.UserType
import com.trinitcore.asd.user.configurations.Column
import com.trinitcore.sqlv2.queryObjects.builders.ModuleTableBuilder
import com.trinitcore.sqlv2.queryObjects.builders.TableBuilder
import com.trinitcore.sqlv2.queryUtils.associationV2.Associating
import com.trinitcore.sqlv2.queryUtils.associationV2.format.DateFormatAssociation
import com.trinitcore.sqlv2.queryUtils.associationV2.table.RowsAssociation
import com.trinitcore.vira.modules.Concert
import com.trinitcore.vira.modules.Photo
import com.trinitcore.vira.users.admin.AdminUserType
import com.trinitcore.vira.users.client.ClientUserType
import kotlin.reflect.KClass

class ClientAdminUserType() : CommonUserType(ClientUserType::class, AdminUserType::class) {
    override fun configureUserTable(userTableBuilder: TableBuilder): Boolean {
        return true
    }

    override fun tables(): Array<TableBuilder> = arrayOf(
            ModuleTableBuilder("concert", moduleInitialisation = { Concert() })
                    .addAssociation(DateFormatAssociation("formattedDateTime","dd/MM/yyyy"))
                    .addAssociation(DateFormatAssociation("formattedCreationDateTime","dd/MM/yyyy"))
                    .addAssociation(RowsAssociation("photo", Associating("ID","photos","concertID").blankRowsIfMatchNotFound()).module { Photo() })
    )

    override fun userColumns(): Array<Column>? = null

}