package com.trinitcore.vira.users

import com.trinitcore.asd.user.User
import com.trinitcore.asd.user.UserType
import com.trinitcore.sqlv2.queryUtils.module.Attribute

open class GenericUser(userType: UserType<*>) : User(userType) {

    var firstname: String by Attribute()
    var lastname: String by Attribute()
    var email: String by Attribute()

}