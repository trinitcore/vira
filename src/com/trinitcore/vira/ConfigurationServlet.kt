package com.trinitcore.vira

import com.trinitcore.asd.Configuration
import com.trinitcore.asd.resourceTools.email.Email
import com.trinitcore.asd.resourceTools.email.configurations.GoogleMailEmailConfiguration
import com.trinitcore.asd.resourceTools.email.configurations.SMTPEmailConfiguration
import com.trinitcore.asd.user.CommonUserType
import com.trinitcore.asd.user.PublicUserType
import com.trinitcore.asd.user.UserType
import com.trinitcore.asd.user.configurations.ColumnType
import com.trinitcore.asd.user.configurations.predefined.EmailColumn
import com.trinitcore.asd.user.configurations.predefined.PasswordColumn
import com.trinitcore.sqlv2.queryObjects.builders.TableBuilder
import com.trinitcore.sqlv2.queryUtils.connection.ConnectionManager
import com.trinitcore.sqlv2.queryUtils.connection.PostgresConnectionManager
import com.trinitcore.vira.users.PublicType
import com.trinitcore.vira.users.admin.AdminUserType
import com.trinitcore.vira.users.client.ClientUserType
import com.trinitcore.vira.users.common.ClientAdminUserType
import javax.servlet.annotation.WebListener
import javax.servlet.http.HttpServlet
import javax.swing.Action

@WebListener
class ConfigurationServlet : HttpServlet() {

    init {
        Configuration.setupConfiguration(ASDConfiguration())
    }

    class ASDConfiguration : Configuration() {

        override fun configureLoginUserTable(userTableBuilder: TableBuilder) {

        }

        override fun databaseConnection(): ConnectionManager = PostgresConnectionManager("localhost","vira","postgres","@C[]4m9c17",false)

        override fun defineActions(): Array<Action> = arrayOf()

        override fun defineCommonUserTypes(): Array<CommonUserType> = arrayOf(
                ClientAdminUserType()
        )

        override fun defineEmailConfiguration(): SMTPEmailConfiguration = GoogleMailEmailConfiguration("","","","emailschedule")

        override fun defineFileStoragePath(): String = "/uploadedFiles/vira"

        override fun defineLoginColumns(): Array<ColumnType> = arrayOf(
                EmailColumn().unique(),
                PasswordColumn()
        )

        override fun definePublicUserType(): PublicUserType = PublicType()

        override fun defineResourcesDirectory(): String = "resources"

        override fun defineUserTypes(): Array<UserType<*>> = arrayOf(
                ClientUserType(), AdminUserType()
        )

        override fun projectTitle(): String = "Virra Marketing"

        override fun resetPasswordLinkSuffix(resetKey: String, userID: Int?): String = "/passwordReset/$userID/$resetKey"

        override fun websiteLinkPrefix(): String = "http://virramarketing.com"

    }

}